# Projectboost

A simple game designed in Godot while taking a Udemy class.

The game consists of flying your rocket from the launchpad to the landing pad. If you collide with any other objects, your rocket will explode. If you land on the landing pad, you will proceed to the next level.

There are only four levels and then the game repeats.

This is not a polished game. Just a tool for learning.

The controls are the arrow keys, or you may click on the buttons on the screen.

Using Godot, this game may be built for Android, HTML5, Windows, and Linux.
