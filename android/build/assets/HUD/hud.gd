extends Node2D
@onready var thruster_button: TouchScreenButton = $ThrusterButton
@onready var left_button: TouchScreenButton = $LeftButton
@onready var right_button: TouchScreenButton = $RightButton
@onready var thruster_2_button: Button = $ThrusterButton/Thruster2Button
@onready var left_2_button: Button = $LeftButton/Left2Button
@onready var right_2_button: Button = $RightButton/Right2Button


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if thruster_button.is_pressed():
		Input.action_press("ui_copy")
	else:
		Input.action_release("ui_copy")
	
	if left_button.is_pressed():
		Input.action_press("ui_cut")
	else:
		Input.action_release("ui_cut")
	
	if right_button.is_pressed():
		Input.action_press("ui_paste")
	else:
		Input.action_release("ui_paste")
	
	if thruster_2_button.is_pressed():
		Input.action_press("ui_copy")
	else:
		Input.action_release("ui_copy")
	
	if left_2_button.is_pressed():
		Input.action_press("ui_cut")
	else:
		Input.action_release("ui_cut")
	
	if right_2_button.is_pressed():
		Input.action_press("ui_paste")
	else:
		Input.action_release("ui_paste")
